# -----------------------------------------------------------
# Aire Logic Tech Test
#
# Interact With APIs
# This program, when given the name of an artist, will produce the average
# number of words in their songs.
#
# Kenny Thompson
# -----------------------------------------------------------
import json
import requests
import urllib


class Lyrics:
    """
    Class to hold the lyrics of a song
    """
    def __init__(self, data):
        self.__dict__ = data

    def get_words(self):
        """
        Return the words of the lyrics
        :return:
        """
        return self.lyrics


class LyricsFactory:
    """
    A factory class to create the lyrics of a song
    This factory retrieves the lyrics from LYRIC_URL
    """
    LYRIC_URL = 'https://api.lyrics.ovh/v1/'

    def _sanitise(self, query):
        """
        Convert to lower case and swap space for %20
        :param query: Query string to sanitise
        :return: sanitised query string
        """
        query = urllib.parse.quote_plus(query)
        return query

    def create_lyrics(self, artist_name, title):
        """
        Return a dict converted from the json of the
        lyrics returned from LYRIC_URL
        :param artist_name: Name of the artist
        :param title: Name of the song
        :return: dict - lyrics dictionary returned from LYRIC_URL
        """
        request = '{endpoint}/{artist}/{song}'.format(
            endpoint=self.LYRIC_URL,
            artist=self._sanitise(artist_name),
            song=self._sanitise(title)
        )
        try:
            # Set timeout to 2 seconds to stop it running a long time
            response = requests.get(request, timeout=2)
        except requests.exceptions.RequestException as e:
            # Handle connection error
            print("Failed to connect to API Lyrics Server")
            raise SystemExit(e)

        try:
            lyrics = Lyrics(response.json())
        except json.decoder.JSONDecodeError as e:
            # Json decode error, set lyrics to nothing
            lyrics = {"lyrics": ""}
        return lyrics


class Song(LyricsFactory):
    """
    Class to hold information about a song
    """

    def __init__(self, data, artist_name):
        self.__dict__ = data
        self.artist_name = artist_name
        self._lyrics = None

    def get_title(self):
        """
        Return the title of the song
        :return: string - title of song
        """
        return self.title

    def get_lyrics(self):
        """
        Retrieve the lyrics from the server
        :return: Lyrics object
        """
        if self._lyrics is None:
            self._lyrics = self.create_lyrics(self.artist_name, self.get_title())

        return self._lyrics

    def get_num_words(self):
        """
        Calculate the number of words in the song
        :return: int: Number of words in the contained lyrics
        """
        lyrics = self.get_lyrics()
        return len(lyrics.get_words().split())


class SongsFactory:
    """
    A factory class to create the songs by an artist
    This factory retrieves the songs list from SONG_LIST_URL
    """
    SONG_LIST_URL = 'https://musicbrainz.org/ws/2/artist/'

    @staticmethod
    def _sanitize_song_title_key(title):
        """
        Lower case and remove whitespace of song title
        to be used as dictionary key
        :param title: String title of the song
        :return: String sanitised song title
        """
        title = title.lower()
        title = title.replace(' ', '')
        return title

    def create_songs(self, id, artist_name):
        """
        Create a dict of Song objects by parsing the
        json returned from the SONG_LIST_URL

        :param id: string - id of this artist
        :param artist_name: - string - name of the artist
        :return: dict - dictionary of Songs
        """
        songs = {}
        request = '{endpoint}{artist_id}?inc=work-rels&fmt=json'.format(
            endpoint=self.SONG_LIST_URL,
            artist_id=id
        )

        try:
            response = requests.get(request)
        except requests.exceptions.RequestException as e:
            # Handle connection error
            print("Failed to connect to Music Brainz Server")
            raise SystemExit(e)

        song_list = response.json()

        # To access song json is formatted:
        # relations -> work
        for song_dict in song_list['relations']:
            song = Song(song_dict['work'], artist_name)
            songs[self._sanitize_song_title_key(song.title)] = song

        return songs


class Artist(SongsFactory):
    """
    Class to hold information about artist
    """
    def __init__(self, data):
        self.__dict__ = data
        self._songs = {}

    def get_name(self):
        """
        Return the name of the artist
        :return: string - name of artist
        """
        return self.name

    def get_id(self):
        """
        Return the id of the artist
        :return: string - id
        """
        return self.id

    def get_songs(self):
        """
        Retrieve artist's songs from server
        Don't allow repeat songs (Two songs with the same name)
        :return: dict: Dictionary of Song objects
        """
        if len(self._songs) == 0:
            self._songs = self.create_songs(self.get_id(), self.get_name())
        return self._songs

    def get_mean_words(self):
        """
        Calculate the mean number of words in this artists songs
        Only count songs with words in them
        :return:
        int: mean number of words
        """
        print("Calculating mean number of words. This might take a while")
        total_num_words = 0
        songs_with_words = 0

        if len(self.get_songs()) == 0:
            print("This artist currently doesn't have any songs")
            return 0

        for song in self._songs:
            num_words = self._songs[song].get_num_words()
            # Lyrics server can return songs with 0 lyrics
            # Even though they are meant to have lyrics
            # Discard all songs without lyrics
            if num_words > 0:
                total_num_words += num_words
                songs_with_words += 1

        # calculate average by total words / number of songs
        average = 0
        if total_num_words > 0 and songs_with_words > 0:
            # Round number to nearest int, as words are whole
            average = round(total_num_words / songs_with_words)

        return average

    def get_shortest_song(self):
        """
        Get the song with the least number of words
        Only count songs with words in them
        :return:
        string: song title
        int: number of words
        """
        if len(self.get_songs()) == 0:
            print("This artist currently doesn't have any songs")

        num_words = None
        song_title = None

        for song in self._songs:
            words = self._songs[song].get_num_words()
            # Lyrics server can return songs with 0 lyrics
            # Even though they are meant to have lyrics
            # Discard all songs without lyrics
            if words > 0:
                if num_words is None or num_words > words:
                    num_words = words
                    song_title = self._songs[song].title

        return song_title, num_words

    def get_longest_song(self):
        """
        Get the song with the most number of words
        :return:
        string: song title
        int: number of words
        """
        if len(self.get_songs()) == 0:
            print("This artist currently doesn't have any songs")

        num_words = None
        song_title = None

        num_words = 0
        song_title = None

        for song in self._songs:
            words = self._songs[song].get_num_words()
            if words > num_words:
                num_words = words
                song_title = self._songs[song].title

        return song_title, num_words


class ArtistFactory:
    """
    Factory class to create artists from the json
    retrieved from ARTIST_URL
    """

    ARTIST_URL = 'https://musicbrainz.org/ws/2/artist/'

    @staticmethod
    def _sanitize_artist_query(name):
        """
        URL Encode the artist query
        :return:
        string: URL encoded string
        """
        return urllib.parse.urlencode({"query": name})

    @staticmethod
    def _filter_disambiguation(artists_json, name):
        """
        Filter artists with similar names using disambiguation tag
        :return: JSON: Filtered artist JSON information
        """
        results = list(enumerate(artists_json['artists']))

        if len(results) == 0:
            print("No artists found with the name:", name)
            raise SystemExit()

        if len(results) > 1:
            # If multiple artists have been returned in the search
            # let the user chose which one
            for n, artist in results:
                output = '{n}: {name}'.format(n=n, name=artist['name'])
                print(output)

            print("Multiple artists found. Which one did you mean?")
            # sanitise user input
            while True:
                try:
                    selection = int(input("Please enter a number: "))
                except ValueError:
                    print("Sorry, you must enter a number")
                    # Prompt user again:
                    continue
                if selection < 0 or selection > len(results):
                    # Number not in range
                    prompt = 'Please select a number between 0 and {len}'.format(len=len(results))
                    print(prompt)
                    # Prompt user again:
                    continue
                else:
                    # All valid user input
                    break

            artist = results[selection][1]
            print("You have selected: ", artist['name'])
        else:
            artist = artists_json['artists']

        return artist

    def create(self, name):
        """
        Create Artist object from json data converted to dict
        retrieved from ARTIST_URL
        :param name: name pf artist
        :return: Artist
        """
        # Get the list of artists with matched name
        request = '{endpoint}?{query}&fmt=json'.format(
            endpoint=self.ARTIST_URL,
            query=self._sanitize_artist_query(name)
        )

        try:
            # Set timeout to 2 seconds to stop it running a long time
            response = requests.get(request, timeout=2)
        except requests.exceptions.RequestException as e:
            # Handle connection error
            print("Failed to connect to Music Brainz Server")
            raise SystemExit(e)

        artist_dict = self._filter_disambiguation(response.json(), name)
        return Artist(artist_dict)


def main():
    artist_name = input("Please enter an artists name: ")
    # Create Artist Factory
    artist_factory = ArtistFactory()
    # Create the artist
    artist = artist_factory.create(artist_name)

    # Print mean number of words
    print(artist.get_name(), "has an average of", artist.get_mean_words(), "words per song")

    # Print longest song
    long_song_title, long_song_word = artist.get_longest_song()
    longest_song = "{artist}'s longest song is '{song}' with {num} words.".format(
        artist=artist.get_name(),
        song=long_song_title,
        num=long_song_word
    )
    print(longest_song)

    # Print shortest song
    short_song_title, short_song_word = artist.get_shortest_song()
    shortest_song = "{artist}'s shortest song is '{song}' with {num} words.".format(
        artist=artist.get_name(),
        song=short_song_title,
        num=short_song_word
    )
    print(shortest_song)


if __name__ == "__main__":
    main()

