# Mean Songs
Aire Logic Tech Test - Interact With APIs.

This program, when given the name of an artist, will produce the average number of words in their songs.

It also returns extra statics about the artist - Longest song, shortest song

## Assumptions
1. The average number of words returned is rounded to the nearest integer. This is because words are whole.
2. The Music Brainz Server can return multiple duplicates of the same song. Duplicates are filtered out by using a unique Python dictionary
3. Only calculate songs that have lyrics - api.lyrics.ovh can return empty strings for songs that actually have lyrics. As a crude implementation, songs with 0 words are discarded from the count.
4. The request timeout has been reduced to 2 seconds to fail faster.

## Prerequisites
Mean Songs requires Python3+ to be installed on your system

## Set Up
The following commands are writen for a bash shell. 

1: Create a virtual environment
```
python -m venv ./env
```

2: Activate the virtual environment
```
source ./env/bin/activate 
```

3: Install the library requirements using pip
```
pip install -r requirements.txt
```

## Running The Program
```
python mean_songs.py
```
The program will then prompt for user input on the CLI.

## Running The Tests
```
python tests.py
```

