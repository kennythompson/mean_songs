# -----------------------------------------------------------
# Aire Logic Tech Test
#
# Interact With APIs
# Unit tests for mean_song.py
#
# Kenny Thompson
# -----------------------------------------------------------

import unittest

from mean_songs import Artist, ArtistFactory, Song, SongsFactory, Lyrics


class TestMeanSongMethods(unittest.TestCase):

    def setUp(self):
        """
        Set up for the Artist tests
        :return:
        """
        # Create dummy artist data
        self.dummy_artist_name = "Dummy Artist"
        self.dummy_artist_data = {
            "name": self.dummy_artist_name,
            "id": "123456"
        }

        # Create dummy artist
        self.dummy_artist = Artist(self.dummy_artist_data)

        # Create dummy songs
        # Song 1
        self.dummy_song_data_1 = {
            "title": "Dummy Song 1"
        }
        self.song1 = Song(self.dummy_song_data_1, self.dummy_artist_name)
        self.lyrics1 = Lyrics({"lyrics": "one"})
        self.song1._lyrics = self.lyrics1
        self.song1_len = 1

        # Song 2
        self.dummy_song_data_2 = {
            "title": "Dummy Song 2"
        }
        self.song2 = Song(self.dummy_song_data_2, self.dummy_artist_name)
        self.lyrics2 = Lyrics({"lyrics": "one two"})
        self.song2._lyrics = self.lyrics2
        self.song2_len = 2

        # Song 3
        self.dummy_song_data_3 = {
            "title": "Dummy Song 3"
        }
        self.song3 = Song(self.dummy_song_data_3, self.dummy_artist_name)
        self.lyrics3 = Lyrics({"lyrics": "one two three"})
        self.song3._lyrics = self.lyrics3
        self.song3_len = 3

        self.longest_word_count = 3
        self.shortest_word_count = 1

        # Add songs to artist
        self.dummy_artist._songs["song1"] = self.song1
        self.dummy_artist._songs["song2"] = self.song2
        self.dummy_artist._songs["song3"] = self.song3

    def test_longest_song_api(self):
        """
        Test get_longest_song returns the longest song name and word count
        """
        name, count = self.dummy_artist.get_longest_song()
        self.assertEqual(count, self.longest_word_count)
        self.assertEqual(name, self.dummy_song_data_3['title'])

    def test_shortest_song_api(self):
        """
        Test get_shortest_song returns the shortest song name and word count
        """
        name, count = self.dummy_artist.get_shortest_song()
        self.assertEqual(count, self.shortest_word_count)
        self.assertEqual(name, self.dummy_song_data_1['title'])

    def test_mean_word_api(self):
        """
        Test get_mean_words returns the average word count
        """
        self.assertEqual(self.dummy_artist.get_mean_words(), 2)

    def test_shortest_disregard_0_count(self):
        """
        Test get_shortest_song disregards 0 word songs
        """
        dummy_song_data_0 = {
            "title": "Dummy Song 0"
        }
        song0 = Song(dummy_song_data_0, self.dummy_artist.get_name())
        lyrics0 = Lyrics({"lyrics": ""})
        song0._lyrics = lyrics0
        self.dummy_artist._songs["song0"] = song0

        name, count = self.dummy_artist.get_shortest_song()
        self.assertEqual(count, self.shortest_word_count)
        self.assertEqual(name, self.dummy_song_data_1['title'])

    def test_mean_word_disregard_0_count(self):
        """
        Test get_mean_words disregards 0 word songs
        """
        dummy_song_data_0 = {
            "title": "Dummy Song 0"
        }
        song0 = Song(dummy_song_data_0, self.dummy_artist.get_name())
        lyrics0 = Lyrics({"lyrics": ""})
        song0._lyrics = lyrics0
        self.dummy_artist._songs["song0"] = song0

        self.assertEqual(self.dummy_artist.get_mean_words(), 2)

    def test_no_artist_found(self):
        """
        Test the program exits cleanly if an artist isn't found in the database
        """
        with self.assertRaises(SystemExit):
            artist_factory = ArtistFactory()
            # Force fake url to simulate address fail
            artist_factory.create("FAKEFAKEFAKE")

    def test_handle_connection_fail(self):
        """
        Test the program exits cleanly if connection to the database fails
        """
        with self.assertRaises(SystemExit):
            artist_factory = ArtistFactory()
            # Force fake url to simulate address fail
            artist_factory.ARTIST_URL = "https://fakeaddresshere.blahblah"
            artist_factory.create("John Lennon")

    def test_sanitize_artist_url(self):
        """
        Test artist name is encoded correctly
        """
        self.assertEqual(ArtistFactory._sanitize_artist_query("Beastie Boys"), 'query=Beastie+Boys')

    def test_sanitize_song_title_key(self):
        """
        Test artist name is encoded correctly
        """
        self.assertEqual(SongsFactory._sanitize_song_title_key("Test Song Name &"), 'testsongname&')

    def test_song_names_with_special_characters(self):
        """
        Test a song name with special characters
        Song - 1%
        Artist - Funeral For A Friend
        """
        dummy_song_data = {
            "title": "1%"
        }
        song = Song(dummy_song_data, "Funeral For A Friend")
        words = song.get_num_words()
        self.assertGreater(words, 0)


if __name__ == '__main__':
    unittest.main()

